﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Quake2Launcher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private const string PATH_FILE = "gamepath.txt";

        ProcessStartInfo start = new ProcessStartInfo(); // Handles launch parameters
        string pathToGame; // Path to game executable

        public MainWindow()
        {
            InitializeComponent();

            Microsoft.Win32.OpenFileDialog gamePathDialog = new Microsoft.Win32.OpenFileDialog();
            gamePathDialog.Title = "Select vkQuake2 executable...";
            gamePathDialog.FileName = "quake2.exe";
            gamePathDialog.DefaultExt = ".exe";
            gamePathDialog.Filter = "Game Executable (.exe)|*.exe";

            if (File.Exists(PATH_FILE) == true)
                pathToGame = File.ReadAllText(PATH_FILE);
            else
            {
                MessageBox.Show("Game not found. Please select vkQuake2 executable");
                bool pathSelected = false;
                while (pathSelected != true)
                {
                    bool result = (bool)gamePathDialog.ShowDialog();
                    if (result == true)
                    {
                        pathToGame = gamePathDialog.FileName;
                        File.WriteAllText(PATH_FILE, pathToGame);
                        pathSelected = true;
                    }
                    else
                        MessageBox.Show("Executable not selected. Please select executable");
                }

            }

            start.FileName = pathToGame;
            start.WindowStyle = ProcessWindowStyle.Hidden;
            start.UseShellExecute = true;
            start.CreateNoWindow = true;
        }

        private void btnQuake2_Click(object sender, RoutedEventArgs e)
        {
            LaunchGame(start);
        }

        private void btnReckoning_Click(object sender, RoutedEventArgs e)
        {
            start.Arguments = "+set game rogue";

            LaunchGame(start);

        }

        private void btnGroundZero_Click(object sender, RoutedEventArgs e)
        {
            start.Arguments = "+set game xatrix";

            LaunchGame(start);

        }

        private void btnSettings_Click(object sender, RoutedEventArgs e)
        {

        }

        private void LaunchGame(ProcessStartInfo start)
        {
            try
            {
                using (Process proc = Process.Start(start))
                {
                    Application.Current.Shutdown();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
